package com.andriyprokip.shop.manager.interfaces;

import com.andriyprokip.shop.entity.Location;

public interface ILocationManager {

	public Location findByCity(String cityName);

	public void save(Location newLocation);

}
