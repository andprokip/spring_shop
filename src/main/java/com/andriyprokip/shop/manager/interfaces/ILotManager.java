package com.andriyprokip.shop.manager.interfaces;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.stereotype.Component;

import com.andriyprokip.shop.entity.BeanForFilter;
import com.andriyprokip.shop.entity.Category;
import com.andriyprokip.shop.entity.Lot;
import com.andriyprokip.shop.forms.SearchResult;

@Component
public interface ILotManager {

	public void save(List<Lot> lots);

	public void save(Lot lot);

	public Page<Lot> findByUser(Integer userId, int pageNumber, int pageSize,
			String sortingColumn);

	public List<Lot> findAvailableLots();

	public Page<Lot> findAvailableLotsByUser(Integer userId, int pageNumber,
			int pageSize, String sortingColumn);

	public void delete(List<Lot> lots);

	public List<Lot> findAll();

	public Lot find(Integer lotId);

	public Lot findLotByName(String lotName);

	public void changecategory(Category category, Category category1);
	
	public Lot getAvailableLotByUserDateAndName(String lotName,
			 Integer userId);

	public SearchResult getResultOfSearch(BeanForFilter filter, int pageNumber,
			int pageSize, String sortBy, String direction);

	public SearchResult getResultOfSearchInCategory(BeanForFilter filter,
			int pageNumber, int pageSize, String sortBy, String direction);

}
