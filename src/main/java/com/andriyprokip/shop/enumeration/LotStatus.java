package com.andriyprokip.shop.enumeration;
/**
 * Enum represents Lot status
 */
public enum LotStatus {
	AVAILABLE,
	OUT_OF_STOCK
}
