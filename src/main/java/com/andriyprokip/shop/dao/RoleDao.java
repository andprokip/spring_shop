package com.andriyprokip.shop.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.andriyprokip.shop.entity.Role;

@Repository
public interface RoleDao extends JpaRepository<Role, Integer> {

	Role findByName(String name);

}
